package com.retail.utils;
import java.util.Comparator;

import com.retail.model.Item;

public class UPCComparator implements Comparator<Item> {

	//To sort the items based on the Product Code
	public int compare(Item item1, Item item2) {
		if (item1.getProductCode() == item2.getProductCode())
			return 0;
		else if (item1.getProductCode() > item2.getProductCode())
			return 1;
		else
			return -1;
	}
}
