package com.retail.model;

import java.text.DecimalFormat;

public class Item {

	private long productCode;
	private String productDescription;
	private float productPrice;
	private float productWeight;

	public enum ShippingMethod {
		AIR, GROUND;
	}

	private ShippingMethod shippingMethod;

	public Item() {
	}

	public Item(long productCode, String productDescription, float productPrice, float productWeight,
			ShippingMethod shippingMethod) {
		super();
		this.productCode = productCode;
		this.productDescription = productDescription;
		this.productPrice = Float.parseFloat(new DecimalFormat("##.##").format(productPrice));
		this.productWeight = Float.parseFloat(new DecimalFormat("##.##").format(productWeight));
		this.shippingMethod = shippingMethod;
	}

	public long getProductCode() {
		return productCode;
	}

	public void setProductCode(long productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public float getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}

	public float getProductWeight() {
		return productWeight;
	}

	public void setProductWeight(float productWeight) {
		this.productWeight = productWeight;
	}

	public ShippingMethod getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(ShippingMethod shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
}