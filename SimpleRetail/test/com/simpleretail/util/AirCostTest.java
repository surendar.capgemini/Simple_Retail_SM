package com.simpleretail.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.core.AirMethodCostCalculation;
import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;

public class AirCostTest {
	
	private AirMethodCostCalculation testAirCost;
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Before
	public void setUp() {
		testAirCost = new AirMethodCostCalculation();
	}

	@Test
	public void testCalculateCost() {
		assertEquals(4.64, testAirCost.calculateCost(new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR)),.001);
	}
	
	@Test
	public void testCalculateCost1() {
		anException.expect(NullPointerException.class);
		/*anException.expectMessage("Item is Null");
		Item actual = new Item();*/
		throw new NullPointerException("Item is Null");
	}
}
