package com.simpleretail.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.core.GroundMethodCostCalculation;
import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;

public class GroundCostTest {
	
	private GroundMethodCostCalculation testGroundCost;
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Before
	public void setUp() {
		testGroundCost = new GroundMethodCostCalculation();
	}

	@Test
	public void testCalculateCost() {
		assertEquals(1.53, testGroundCost.calculateCost(new Item(567321101986L, "CD - Beatles, Abbey Road", 17.99f, 0.61f, ShippingMethod.GROUND)), .001);
	}
	
	@Test
	public void testCalculateCost1() {
		anException.expect(NullPointerException.class);
		/*anException.expectMessage("Item is Null");
		Item actual = new Item();*/
		throw new NullPointerException("Item is Null");
	}

}
