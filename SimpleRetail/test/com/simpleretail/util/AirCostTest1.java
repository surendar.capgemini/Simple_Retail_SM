package com.simpleretail.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.retail.core.AirMethodCostCalculation;
import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;

@RunWith(Parameterized.class)
public class AirCostTest1 {
	
	private float expectedResult;
	private Item item;
	private static AirMethodCostCalculation airMethodCalculation;
	
	
	public AirCostTest1(float expectedResult, Item item) {
		super();
		this.expectedResult = expectedResult;
		this.item = item;
	}

	@Before
	public void initialize() {
		airMethodCalculation = new AirMethodCostCalculation();
	}

	@Parameterized.Parameters
	public static Collection shippingCost() {
		return Arrays.asList(new Object[][] {
			{4.64, airMethodCalculation.calculateCost(new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR))},
			{6.57, airMethodCalculation.calculateCost(new Item(467321101899L, "iPhone - Waterproof Case", 9.75f, 0.73f, ShippingMethod.AIR))}, 
			{4.40, airMethodCalculation.calculateCost(new Item(567321101985L, "CD - Queen, A Night at the Opera", 20.49f, 0.55f, ShippingMethod.AIR))}
		});
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testCalculateCost() {
		assertEquals(expectedResult, airMethodCalculation.calculateCost(item));
	}
}
