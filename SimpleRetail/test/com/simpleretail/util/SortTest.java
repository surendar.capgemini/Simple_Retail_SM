package com.simpleretail.util;

import static org.junit.Assert.*;

import org.junit.Test;

import com.retail.model.Item;
import com.retail.model.Item.ShippingMethod;
import com.retail.utils.UPCComparator;

public class SortTest {

	private final UPCComparator upcCompare = new UPCComparator();

    @Test
    public void testEqual() {
    	Item item1 = new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR);
    	Item item2 = new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR);
        int result = upcCompare.compare(item1, item2);
        assertTrue("expected to be equal", result == 0);
    }

    @Test
    public void testGreaterThan() {
    	Item item1 = new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR);
    	Item item2 = new Item(567321101986L, "CD - Beatles, Abbey Road", 17.99f, 0.61f, ShippingMethod.GROUND);
        int result = upcCompare.compare(item1, item2);
        assertTrue("expected to be greater than", result >= 1);
    }

    @Test
    public void testLessThan() {
    	Item item1 = new Item(567321101986L, "CD - Beatles, Abbey Road", 17.99f, 0.61f, ShippingMethod.GROUND);
    	Item item2 = new Item(567321101987L, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, ShippingMethod.AIR);
        int result = upcCompare.compare(item1, item2);
        assertTrue("expected to be less than", result <= -1);
    }
}
